package no.uib.inf101.sample.Game.View;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import no.uib.inf101.sample.Game.model.Person;

public class ViewPerson {

    public static void main(String[] args) {

        Person person = new Person("Ola", 20);
        ViewPerson viewPerson = new ViewPerson(person);

        JFrame frame = new JFrame("Mock");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(viewPerson.mainPanel);
        frame.pack();
        frame.setVisible(true);

        
    }
    public JPanel mainPanel;

    
    public ViewPerson( Person person) {

        this.mainPanel = new JPanel();
        this.mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        // alternativer til BoxLayout.PAGE_AXIS
        //cardLayout
        //BoxLayout.LINE_AXIS
        //GridLayout
        //FlowLayout
        //BorderLayout
        //GridBagLayout

        
        this.mainPanel.setBorder(new EmptyBorder(10,10,10,10));

        mainPanel.setPreferredSize(new Dimension(300,400));

        JLabel namLabel = new JLabel("Name: " + person.name());
        JLabel ageLabel = new JLabel("Age: " + person.age());

        // display age below name


        this.mainPanel.add(namLabel);
        this.mainPanel.add(ageLabel);
        //set the backround color of the panel
        this.mainPanel.setBackground(java.awt.Color.lightGray);

        
    }
}
