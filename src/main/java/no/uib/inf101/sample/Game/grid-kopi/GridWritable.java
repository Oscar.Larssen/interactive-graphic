package inf101v22.grid;

import java.util.function.Function;

public interface GridWritable<E> extends GridReadable<E> {

    /**
     * Sets the value of a postion in the grid. A subsequent call to get with an
     * equal coordinate as argument will return the value which was set. The method
     * will overwrite any previous value that was stored at the location.
     * 
     * @param coordinate the location in which to store the value
     * @param value      the new value
     * @throws IndexOutOfBoundsException if the coordinate is not within bounds of
     *                                   the grid
     */
    void set(Coordinate coordinate, E value);

    /**
     * Fills the board with the given value. After the call, every coordinate on the
     * grid will contain the given object.
     * 
     * @param value
     */
    void fill(E value);

    /**
     * Fills the board with values given by the value-producing function. The input
     * provided to the function is the coordinate it will fill.
     * 
     * For example, if you have defined a method
     * 
     * <pre>
     * class MyClass {
     *     static String myFunction(Coordinate coordinate) {
     *         return coordinate.toString();
     *     }
     * }
     * </pre>
     * 
     * you may use this method to fill in a grid {@code IGrid<String> myGrid}
     * as follows: {@code myGrid.fill(MyClass::myFunction)}. Then the grid will be
     * filled with String representations of the different coordinates.
     * </pre>
     * 
     * @param value
     */
    void fill(Function<Coordinate, E> valueProducer);

}
