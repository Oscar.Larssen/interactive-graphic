package inf101v22.grid;

import inf101v22.observable.Observable;

public interface GridObservable<E> extends GridReadable<E> {

 /**
     * Gets an observable for the given coordinate. This allows
     * to add an observer for the specific coordinate in the grid.
     * Note that the observable will notify observers when the
     * object in the grid is replaced, but not if the object mutates.
     * 
     * @param coordinate to be observed
     * @return an observable for the value at the given coordinate
     */
    public Observable<E> getObservable(Coordinate coordinate);
    
    
}
