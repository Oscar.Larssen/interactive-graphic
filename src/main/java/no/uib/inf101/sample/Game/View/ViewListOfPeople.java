package no.uib.inf101.sample.Game.View;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;

import no.uib.inf101.sample.Game.model.Person;
import no.uib.inf101.sample.Game.model.PersonList;

public class ViewListOfPeople {
    private JPanel mainPanel;
    private PersonList model;
    
    ViewListOfPeople view = new ViewListOfPeople(model);
    // make a main method to test the view
    public static void main(String[] args) {

        PersonList model = new PersonList();
        model.addPerson(new Person("Ola", 20));
        model.addPerson(new Person("Kari", 30));

        JFrame frame = new JFrame("Mock");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }



    


    public ViewListOfPeople(PersonList model) {
        this.mainPanel = new JPanel();
        this.model = model;

        


        // make a list of Persons and add them to the model
        DefaultListModel<Person> listModel = new DefaultListModel<Person>();

        for (Person p : model.getPersons()) {
            listModel.addElement(p);
        }
        JList<Person> list = new JList<Person>();

        for (Person p : model.getPersons()) {

            this.mainPanel.add(list);

        }
    }
}