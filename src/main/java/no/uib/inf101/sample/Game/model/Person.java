package no.uib.inf101.sample.Game.model;


public record Person(String name , int age) {
    public Person {
        if (age < 0) {
            throw new IllegalArgumentException("Age must be positive");
        }
    }
}


    

